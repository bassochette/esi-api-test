import { Application } from '../../lib/Application';
import { CharacterDao } from '../../lib/data/CharacterDao';

describe('CharacterDao', () => {
    const app = new Application();
    let dao: CharacterDao;

    before(async () => {
        await app.initialize();
        dao = app.dependencies.dao.characterDao;
    });

    after(async () => {
        await app.shutdown();
    });

    it('connects', async () => {
        await dao.delete('123456');
        const all = await dao.getAll();
        console.log(`ALL`, all);
    });
});
