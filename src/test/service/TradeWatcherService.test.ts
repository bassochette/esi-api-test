import * as _ from 'lodash';
import chalk from 'chalk';

import { Application } from '../../lib/Application';
import { TradeWatcherService } from '../../lib/service';
import { IConfig } from '../../lib/config';

describe('TradeWatcherService', async function () {
    this.timeout(0);

    const app = new Application();
    let config: IConfig;
    let service: TradeWatcherService;

    before(async () => {
        await app.initialize();
        service = app.dependencies.services.tradeWatcherService;
        config = app.dependencies.common.config;
    });

    after(async () => {
        await app.shutdown();
    });

    it('get current Amarr trades WatchReports', async () => {
        await refresh();
        setInterval(refresh, 5 * 60 * 1000 + 3000);
        return new Promise(resolve => {});
    });

    async function refresh() {
        // console.log(`\n\n\n\n\n\n\n${new Date()}\n`);
        // const reports = await service.watchTrades(config.test.characterId, Station.AMARR_HUB);
        // const ordersToUpdate = _.filter(reports, r => !r.isBestSellOrder);
        // console.log(`ORDERS TO UPDATE`);
        // ordersToUpdate.length === 0 && console.log(`[No order to update]`);
        // ordersToUpdate.forEach((r) => {
        //     const color = r.updatable ? 'green' : 'yellow';
        //     return console.log(`=> ${chalk[color](r.type.name)} : ${_.round(r.bestSellPrice - r.order.price, 2)}`);
        // });
    }
});
