import { Application } from '../../lib/Application';
import { TradeOpportunitiesService } from '../../lib/service';

describe('TradeOpportunitiesService', async function () {
    this.timeout(0);
    const app = new Application();
    let service: TradeOpportunitiesService;

    before(async () => {
        await app.initialize();
        service = app.dependencies.services.tradeOpportunitiesService;
    });

    after(async () => {
        await app.shutdown();
    });
});
