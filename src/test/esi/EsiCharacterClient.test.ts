import { assert } from 'chai';

import { Application } from '../../lib/Application';
import { EsiCharacterClient } from '../../lib/esi';
import { IConfig } from '../../lib/config';

const logger = require('../../lib/util/Logger')(module);

describe('EsiCharacterClient', function () {
    this.timeout(60000);

    const app = new Application();
    let config: IConfig;
    let client: EsiCharacterClient;

    before(async () => {
        await app.initialize();
        config = app.dependencies.common.config;
        client = app.dependencies.esiClients.esiCharacterClient;
    });

    after(async () => {
        await app.shutdown();
    });

    it('Get the public data of a character', async () => {
        const character = await client.getCharacter(config.test.ids.eloKhamez);
        assert.isString(character.name);
        assert.isNumber(character.security_status);
        assert.isString(character.birthday);
        // logger.info(JSON.stringify(character, null, 2));
    });
});
