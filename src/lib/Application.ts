import * as bluebird from 'bluebird';

import { DependenciesBuilder, IDependencies, ILifeCycleAware } from './util';
import { config, IConfig } from './config';

const logger = require('./util/Logger')(module);

export class Application implements ILifeCycleAware {
    public dependencies: IDependencies;
    private lifeCycleAwareComponent: ILifeCycleAware[] = [];

    public async initialize(conf: IConfig = config) {
        logger.info('Initializing dependencies');
        this.dependencies = new DependenciesBuilder().build(conf);

        logger.info('Initializing components');
        this.lifeCycleAwareComponent = [
            ...Object.values(this.dependencies.dao),
            this.dependencies.esiClients.esiAuthClient,
            this.dependencies.http.server,
        ];
        await bluebird.map(this.lifeCycleAwareComponent, component => component.initialize());
    }

    public async shutdown() {
        logger.info(`Shutting down components`);
        await bluebird.map(this.lifeCycleAwareComponent, component => component.shutdown());
    }
}
