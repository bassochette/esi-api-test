import { EntityId, IEsiCharacterPortrait, IEsiCharacterPublicData } from '../../esi/types';

export interface ICharacter {
    id: EntityId;
    data: IEsiCharacterPublicData;
    portraits: IEsiCharacterPortrait;
    walletBalance: number;
}
