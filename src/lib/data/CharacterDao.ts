import { ICommonDeps } from '../util';
import { AbstractDao } from './AbstractDao';
import { ICharacter } from './types';

export class CharacterDao extends AbstractDao<ICharacter> {
    private static readonly COLLECTION = 'characters';

    constructor(commonDeps: ICommonDeps) {
        super(commonDeps, CharacterDao.COLLECTION);
    }
}
