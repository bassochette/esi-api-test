import { AbstractDao } from './AbstractDao';
import { ICommonDeps } from '../util';
import { ITradeOpportunitiesOptions } from '../service';
import { DateString, EntityId } from '../esi';

export interface ITradeOpportunities {
    id: string;
    date: DateString;
    options: ITradeOpportunitiesOptions;
    srcRegionId: number;
    srcRegionName: string;
    dstRegionId: number;
    dstRegionName: string;
    opportunities: ITradeOpportunity[];
}

export interface ITradeOpportunity {
    typeId: EntityId;
    groupId?:  EntityId;
    typeName: string;
    groupName?: string;
    volume: number;
    src: ITypeTradeStats;
    dst: ITypeTradeStats;

    tradeValue: number;
}

export interface ITypeTradeStats {
    typeId: EntityId;
    regionId: EntityId;
    minSellPrice: number;
    availableVolumeAt5Percent: number;
    tradedVolumeByDay: number;
    tradesByDay: number;
    averageRecentPrice: number;
}

export class TradeOpportunitiesDao extends AbstractDao<ITradeOpportunities> {
    private static readonly COLLECTION = 'trade-opportunities';

    constructor(commonDeps: ICommonDeps) {
        super(commonDeps, TradeOpportunitiesDao.COLLECTION);
    }
}
