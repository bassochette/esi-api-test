import { Router } from 'express';

import { IRouter } from './HttpServer';
import { CharacterDao } from '../data';
import { EsiDataService } from '../service';

export class DataRouter implements IRouter {
    public readonly router: Router;

    private characterDao: CharacterDao;
    private esiDataService: EsiDataService;

    constructor(characterDao: CharacterDao, esiDataService: EsiDataService) {
        this.characterDao = characterDao;
        this.esiDataService = esiDataService;
        this.router = this.buildRouter();
    }

    private buildRouter(): Router {
        const router = Router();

        router.get('/regions', async (req, res) => {
            const regions = await this.esiDataService.getRegions();
            res.json(regions);
        });

        router.get('/stations/hubs', async (req, res) => {
            const hubs = await this.esiDataService.getTradeHubs();
            res.json(hubs);
        });

        router.get('/categories/groups', async (req, res) => {
            const data = await this.esiDataService.getGroupsByCategory();
            res.json(data);
        });

        return router;
    }
}
