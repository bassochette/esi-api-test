import { Router } from 'express';
import * as SseChannel from 'sse-channel';
import { v4 as uuid } from 'uuid';

import { IRouter } from './HttpServer';
import { EventHub, EventName, IEventHubListener } from '../util';

const logger = require('../util/Logger')(module);

export class EventsRouter implements IRouter, IEventHubListener {
    public readonly router: Router;
    private sseChannel: SseChannel;

    constructor(eventHub: EventHub) {
        this.router = this.buildRouter();
        this.sseChannel = new SseChannel({
            cors: { origins: '*' },
        });

        eventHub.setListener(this);
    }

    public publish(eventName: EventName, eventData: any): void {
        logger.silly(`publishing event ${eventName}`);
        this.sseChannel.send({
            id: uuid(),
            event: eventName,
            data: eventData,
        });
    }

    private buildRouter(): Router {
        const router = Router();
        router.get('/events', async (req, res) => {
            logger.silly(`Registering SSE Client from ${req.ip}`);
            this.sseChannel.addClient(req, res);
        });
        return router;
    }
}
