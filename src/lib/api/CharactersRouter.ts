import { Router } from 'express';

import { IRouter } from './HttpServer';
import { IConfig } from '../config';
import { CharacterDao } from '../data';
import { EsiAuthClient } from '../esi';
import { CharacterService } from '../service';
import { EventHub, EventName } from '../util';

export class CharactersRouter implements IRouter {
    public readonly router: Router;
    private config: IConfig;
    private eventHub: EventHub;
    private characterDao: CharacterDao;
    private characterService: CharacterService;
    private esiAuthClient: EsiAuthClient;

    constructor(config: IConfig, eventHub: EventHub, characterDao: CharacterDao, characterService: CharacterService,
                esiAuthClient: EsiAuthClient) {
        this.config = config;
        this.eventHub = eventHub;
        this.characterDao = characterDao;
        this.characterService = characterService;
        this.esiAuthClient = esiAuthClient;
        this.router = this.buildRouter();
    }

    private buildRouter(): Router {
        const router = Router();

        router.get('/characters', async (req, res) => {
            const chars = await this.characterDao.getAll();
            res.json(chars);
        });

        router.post('/characters', async (req, res) => {
            this.esiAuthClient.authenticateNewCharacter(this.config.esi.scopes)
                .then(character => this.eventHub.publish(EventName.CHARACTER_UPDATED, character))
                .catch(err => this.eventHub.publish(EventName.CHARACTER_AUTH_ERROR, err));

            res.sendStatus(202);
        });
        router.post('/characters/:characterId/refresh', async (req, res, next) => {
            try {
                const { characterId } = req.params;
                const character = await this.characterService.updateCharacter(parseInt(characterId));
                res.send(character);
            } catch (e) {
                next(e);
            }
        });

        router.delete('/characters/:characterId', async (req, res) => {
            const { characterId } = req.params;
            await this.characterService.deleteCharacter(parseInt(characterId));
            res.sendStatus(204);
        });

        return router;
    }
}
