import * as _ from 'lodash';

export interface IObservable<Type> {
    on<Event>(eventName: Type, callback: IObservableCallback<Type, Event>): void;
}

export type IObservableCallback<EventType, T> = (eventName: EventType, event: T) => any;

export class ObservableUtils<EventType extends string> {
    private readonly listeners: { [k: string]: IObservableCallback<any, any>[] };

    constructor() {
        this.listeners = {};
    }

    public register<Event>(eventName: EventType, callback: IObservableCallback<EventType, Event>): void {
        if (!this.listeners[eventName]) {
            this.listeners[eventName] = [];
        }
        this.listeners[eventName].push(callback);
    }

    public trigger<Event = void>(eventName: EventType, event?: Event): void {
        _.each(this.listeners[eventName] || [], (callback: IObservableCallback<EventType, Event>) => callback(eventName, event));
    }
}
