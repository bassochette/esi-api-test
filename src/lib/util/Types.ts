
export interface IKeyValue<T> {
    [key: string]: T;
}

export interface ILifeCycleAware {
    initialize(): Promise<void>;

    shutdown(): Promise<void>;
}

export interface IProgressAware {
    progress(current: number, total: number|null);
}

export interface IProgressStage {
    stage: number;
    label: string;
    progress: number;
    total: number|null;
    current: number;
}

export interface IMultiStageProgressAware {
    progress(progresses: IProgressStage[]);
}

export const voidProgress = { progress: () => null };
