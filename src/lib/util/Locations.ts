
export enum Region {
    DOMAIN = 10000043,
    HEIMATAR = 10000030,
    METROPOLIS = 10000042,
    THE_FORGE = 10000002,
}

export enum Station {
    AMARR_HUB = 60008494,
    JITA_HUB = 60003760,
    DODIXIE_HUB = 60011866,
}

export const TRADE_HUBS = {
    AMARR_HUB: Station.AMARR_HUB,
    JITA_HUB: Station.JITA_HUB,
    DODIXIE_HUB: Station.DODIXIE_HUB,
};
