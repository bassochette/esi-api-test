import * as bluebird from 'bluebird';
import * as _ from 'lodash';

import { EntityId, EsiUniverseClient, IEsiCategory, IEsiGroup, IEsiRegion, IEsiStation } from '../esi';
import { TRADE_HUBS } from '../util';

export interface ICategoryWithGroups {
    categoryId: EntityId;
    label: string;
    groups: IEsiGroup[];
}

export class EsiDataService {
    private esiUniverseClient: EsiUniverseClient;

    constructor(esiUniverseClient: EsiUniverseClient) {
        this.esiUniverseClient = esiUniverseClient;
    }

    public async getRegions(): Promise<IEsiRegion[]> {
        const regionIds = await this.esiUniverseClient.getRegions();
        return bluebird.map(regionIds, regionId => this.esiUniverseClient.getRegion(regionId));
    }

    public async getTradeHubs(): Promise<IEsiStation[]> {
        return bluebird.map(_.values<number>(TRADE_HUBS), stationId => this.esiUniverseClient.getStation(stationId));
    }

    public async getGroupsByCategory(): Promise<ICategoryWithGroups[]> {
        const categoriyIds = await this.esiUniverseClient.getCategories();
        const categories = await bluebird.map(categoriyIds, id => this.esiUniverseClient.getCategory(id));
        return _.compact(await bluebird.map(categories, async (category: IEsiCategory) => {
            if (!category.published) return null;
            const groups = await bluebird.map(category.groups, groupId => this.esiUniverseClient.getGroup(groupId));
            return {
                categoryId: category.category_id,
                label: category.name,
                groups,
            };
        }));
    }
}
