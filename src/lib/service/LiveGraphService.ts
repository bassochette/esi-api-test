import * as _ from 'lodash';
import { v4 as uuid } from 'uuid';

import { EntityId, EsiMarketClient, IEsiCharacterMarketOrderOpen, EsiWalletClient, EsiUniverseClient, EsiAssetsClient } from '../esi';
import { GraphDao, GraphType, IWorthGraphPoint } from '../data';
import { Duration } from '../util';

const logger = require('../util/Logger')(module);

export class LiveGraphService {
    private graphDao: GraphDao;
    private esiWalletClient: EsiWalletClient;
    private esiMarketClient: EsiMarketClient;
    private esiAssetsClient: EsiAssetsClient;
    private esiUniverseClient: EsiUniverseClient;
    private worthGrapher: NodeJS.Timeout;

    constructor(graphDao: GraphDao, esiWalletClient: EsiWalletClient, esiMarketClient: EsiMarketClient, esiAssetsClient: EsiAssetsClient,
                esiUniverseClient: EsiUniverseClient) {
        this.graphDao = graphDao;
        this.esiWalletClient = esiWalletClient;
        this.esiMarketClient = esiMarketClient;
        this.esiAssetsClient = esiAssetsClient;
        this.esiUniverseClient = esiUniverseClient;
    }

    public async startGrapher(characterId: EntityId): Promise<void> {
        const graph = async () => {
            try {
                const worth = await this.computeWorth(characterId);
                await this.graphDao.insert(worth);
            } catch (e) {
                logger.error(`Error during computeWorth: ${e.message} ${e.stack}`);
            }
        };
        this.worthGrapher = setInterval(graph, 1 * Duration.HOUR);
        await graph();
    }

    private async computeWorth(characterId: EntityId): Promise<IWorthGraphPoint> {
        const walletWorth = await this.esiWalletClient.getWalletBalance(characterId);
        const orders = await this.esiMarketClient.getCharacterOrders(characterId);
        const sellOrdersWorth = _<IEsiCharacterMarketOrderOpen[]>(orders)
            .filter(o => !o.is_buy_order)
            .sumBy(o => o.volume_remain * o.price);
        const assets = await this.esiAssetsClient.getCharacterAssets(characterId);
        const prices = await this.esiMarketClient.getMarketPrices();
        const assetsWorth = _(assets)
            .map(asset => _.find(prices, p => p.type_id === asset.type_id))
            .compact()
            .map(price => price.adjusted_price)
            .sum();

        return {
            id: uuid(),
            date: new Date().toISOString(),
            graphType: GraphType.WORTH,
            characterId,
            walletWorth,
            sellOrdersWorth,
            assetsWorth,
            totalWorth: walletWorth + assetsWorth + sellOrdersWorth,
        };
    }
}
