import { EsiAuthClient } from './';
import { Method } from './AbstractEsiClient';
import { AbstractCachedEsiClient } from './AbstractCachedEsiClient';
import { ICommonDeps, IProgressAware, MultiPageAggregator } from '../util';
import { CacheDao, CacheType } from '../data';
import {
    EntityId,
    EsiMarketOrderType,
    IEsiCharacterMarketOrderCompleted,
    IEsiCharacterMarketOrderOpen,
    IEsiCorpMarketOrderCompleted,
    IEsiCorpMarketOrderOpen,
    IEsiMarketGroupInfo,
    IEsiMarketHistoryPoint,
    IEsiMarketOrderOpen,
    IEsiMarketPrice,
    IEsiStructureMarketOrderOpen,
    Scope,
} from './types';

const logger = require('../util/Logger')(module);

export class EsiMarketClient extends AbstractCachedEsiClient {
    constructor(commonDeps: ICommonDeps, esiAuthClient: EsiAuthClient, cacheDao: CacheDao) {
        super(commonDeps, esiAuthClient, cacheDao);
        this.logger = logger;
    }

    public async getCharacterOrders(characterId: EntityId): Promise<IEsiCharacterMarketOrderOpen[]> {
        return this.queryBody(Method.GET, `/characters/${characterId}/orders`,
            { characterId, scopes: [Scope.MARKETS_READ_CHARACTER_ORDERS], debug: true });
    }

    public async getCharacterCompletedOrders(characterId: EntityId,
                                             progressListener?: IProgressAware): Promise<IEsiCharacterMarketOrderCompleted[]> {
        return MultiPageAggregator.largeAggregate<IEsiCharacterMarketOrderCompleted>(
            (page: number) => this.query(Method.GET, `/characters/${characterId}/orders`, {
                characterId, query: { page },
            }),
            (page: number) => this.cachedQueryBody(CacheType.CHARACTER_ORDERS_HISTORY, `/characters/${characterId}/orders`, {
                characterId,
                query: { page },
            }),
            progressListener);
    }

    public async getCorporationOrders(characterId: EntityId, corporationId: EntityId): Promise<IEsiCorpMarketOrderOpen[]> {
        return MultiPageAggregator.aggregate<IEsiCorpMarketOrderOpen>(
            page => this.queryBody(Method.GET, `/corporations/${corporationId}/orders`, { characterId, query: { page } }));
    }

    public async getCorporationCompletedOrders(characterId: EntityId, corporationId: EntityId): Promise<IEsiCorpMarketOrderCompleted[]> {
        return MultiPageAggregator.aggregate<IEsiCorpMarketOrderCompleted>(
            page => this.queryBody(Method.GET, `/corporations/${corporationId}/orders/history`, { characterId, query: { page } }));
    }

    public async getMarketHistory(regionId: EntityId, typeId: EntityId): Promise<IEsiMarketHistoryPoint[]> {
        return this.cachedQueryBody(CacheType.MARKET_HISTORY,
            `/markets/${regionId}/history`,
            { query: { type_id: typeId }, silent: true });
    }

    public async getMarketOrders(regionId: EntityId,
                                 typeId: EntityId = undefined,
                                 orderType: EsiMarketOrderType = EsiMarketOrderType.ALL,
                                 progressListener?: IProgressAware): Promise<IEsiMarketOrderOpen[]> {
        return MultiPageAggregator.largeAggregate<IEsiMarketOrderOpen>(
            (page: number) => this.query(Method.GET, `/markets/${regionId}/orders`, {
                query: { page, type_id: typeId, order_type: orderType },
            }),
            (page: number) => this.cachedQueryBody(CacheType.MARKET_ORDERS, `/markets/${regionId}/orders`, {
                query: { page, type_id: typeId, order_type: orderType },
            }),
            progressListener);
    }

    public async getMarketTypes(regionId: EntityId, progressListener?: IProgressAware): Promise<EntityId[]> {
        return MultiPageAggregator.largeAggregate<EntityId>(
            page => this.query(Method.GET, `/markets/${regionId}/types`, { query: { page } }),
            page => this.cachedQueryBody(CacheType.MARKET_TYPES, `/markets/${regionId}/types`, { query: { page } }),
            progressListener);
    }

    public getMarketGroups(): Promise<EntityId[]> {
        return this.cachedQueryBody(CacheType.MARKET_GROUPS, `/markets/groups`);
    }

    public getMarketGroupInfo(groupId: EntityId): Promise<IEsiMarketGroupInfo> {
        return this.cachedQueryBody(CacheType.MARKET_GROUP_INFO, `/markets/groups/${groupId}`);
    }

    public getMarketPrices(): Promise<IEsiMarketPrice[]> {
        return this.cachedQueryBody(CacheType.MARKET_PRICE, `/markets/prices`);
    }

    public async getStructureOrders(structureId: EntityId,
                                    page: number = 1): Promise<IEsiStructureMarketOrderOpen[]> {
        return this.queryBody(Method.GET, `/markets/structures/${structureId}`, { query: { page } });
    }
}
