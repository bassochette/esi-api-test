import { DateString, EntityId } from '.';

// ===== Common market types ===== //

export enum EsiMarketOrderType {
    BUY = 'buy',
    SELL = 'sell',
    ALL = 'all',
}

export enum EsiMarketOrderRange {
    J1 = 1,
    J2 = 2,
    J3 = 3,
    J4 = 4,
    J5 = 5,
    J10 = 10,
    J20 = 20,
    J30 = 30,
    J40 = 40,
    STATION = 'station',
    SOLARSYSTEM = 'solarsystem',
    REGION = 'region',
}

interface IEsiMarketOrderCommon {
    duration: number;
    is_buy_order: boolean;
    issued: DateString;
    location_id: EntityId;
    min_volume: number;
    order_id: EntityId;
    price: number;
    range: EsiMarketOrderRange;
    type_id: EntityId;
    volume_remain: number;
    volume_total: number;
}

export enum EsiMarketOrderFinishedState {
    CANCELLED = 'cancelled',
    EXPIRED = 'expired',
}

interface IEsiMarketCompletedOrderDecorator {
    state: EsiMarketOrderFinishedState;
}

interface IEsiPersonalOrderDecorator {
    escrow: number;
    region_id: EntityId;
}

// ===== Character market orders ===== //

export interface IEsiCharacterMarketOrderOpen extends IEsiMarketOrderCommon, IEsiPersonalOrderDecorator {
    is_corporation: boolean;
}

export interface IEsiCharacterMarketOrderCompleted extends IEsiCharacterMarketOrderOpen, IEsiMarketCompletedOrderDecorator {}

// ===== Corporation market orders ===== //

export interface IEsiCorpMarketOrderOpen extends IEsiMarketOrderCommon, IEsiPersonalOrderDecorator {
    issued_by: number;
    wallet_division: number;
}

// ===== Structure market orders ===== //

export interface IEsiStructureMarketOrderOpen extends IEsiMarketOrderCommon {}

export interface IEsiCorpMarketOrderCompleted extends IEsiCorpMarketOrderOpen, IEsiMarketCompletedOrderDecorator {}

// ===== Market stats ===== //

export interface IEsiMarketHistoryPoint {
    average: number;
    date: DateString;
    highest: number;
    lowest: number;
    order_count: number;
    volume: number;
}

export interface IEsiMarketOrderOpen extends IEsiMarketOrderCommon {}

// ===== Market info ===== //

export interface IEsiMarketGroupInfo {
    description: string;
    market_group_id: EntityId;
    name: string;
    parent_group_id: EntityId;
    types: EntityId[];
}

export interface IEsiMarketPrice {
    adjusted_price: number;
    average_price: number;
    type_id: EntityId;
}
