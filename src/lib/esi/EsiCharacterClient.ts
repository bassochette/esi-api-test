import { EsiAuthClient } from './EsiAuthClient';
import { AbstractEsiClient, Method } from './AbstractEsiClient';
import { ICommonDeps } from '../util';
import { EntityId, IEsiCharacterPortrait, IEsiCharacterPublicData, IEsiCharacterStanding } from './types';

const logger = require('../util/Logger')(module);

export class EsiCharacterClient extends AbstractEsiClient {
    constructor(commonDeps: ICommonDeps, esiAuthClient: EsiAuthClient) {
        super(commonDeps, esiAuthClient);
        this.logger = logger;
    }

    public async getCharacter(characterId: EntityId): Promise<IEsiCharacterPublicData> {
        return this.queryBody(Method.GET, `/characters/${characterId}`, { characterId });
    }

    public async getCharacterPortraits(characterId: EntityId): Promise<IEsiCharacterPortrait> {
        return this.queryBody(Method.GET, `/characters/${characterId}/portrait`);
    }

    public async getCharacterStandings(characterId: EntityId): Promise<IEsiCharacterStanding[]> {
        return this.queryBody(Method.GET, `/characters/${characterId}/standings`, { characterId });
    }
}
