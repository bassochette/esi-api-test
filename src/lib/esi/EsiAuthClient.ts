import opn = require('opn');
import * as jwt from 'jwt-decode';
import * as _ from 'lodash';
import * as querystring from 'querystring';
import { Server } from 'http';
import * as express from 'express';
import * as got from 'got';
import chalk from 'chalk';
import * as url from 'url';

import { IConfig } from '../config';
import { EsiHookRouter, IHookCallback, IHookResponse } from './EsiHookRouter';
import { ICommonDeps, ILifeCycleAware } from '../util';
import { EntityId, IAwaitedHook, IToken, Scope } from './types';
import { AuthDao } from '../data';

const logger = require('../util/Logger')(module);

// noinspection UnterminatedStatementJS
export class EsiAuthClient implements ILifeCycleAware {
    private config: IConfig;
    private esiHookRouter: EsiHookRouter;
    private server: Server;
    private awaitedHooks: IAwaitedHook[];
    private authDao: AuthDao;

    constructor(deps: ICommonDeps, esiHookRouter: EsiHookRouter, authDao: AuthDao) {
        this.config = deps.config;
        this.esiHookRouter = esiHookRouter;
        this.awaitedHooks = [];
        this.authDao = authDao;
    }

    public async initialize(): Promise<void> {
        if (this.config.http.headless) return;

        const port = this.config.esi.auth.hookServerPort;
        this.esiHookRouter.onCodeReceived(this.onAuthCodeReceived);
        const app = express();

        app.use(express.json());
        app.use(this.esiHookRouter.buildRouter());
        this.server = app.listen(port);
        logger.info(`Hook server listening on ::${port}`);
    }

    public async shutdown(): Promise<void> {
        logger.info(`Stopping hook server`);
        return new Promise((resolve) => {
            this.server.close(() => resolve());
        });
    }

    public async getAccessToken(characterId: EntityId, scopes: Scope[]): Promise<string> {
        const cached = await this.authDao.get(characterId);
        if (!cached) throw new Error(`Unknown character ${characterId}`);

        const cachedTokenHasSufficientScopes = _.every(scopes, s => _.includes(cached.scopes, s));
        if (cachedTokenHasSufficientScopes && !_.isEmpty(cached.access) && (Date.now() < cached.expires)) {
            logger.silly(`getAccessToken: returning cached token`);
            return cached.access;
        }

        if (cachedTokenHasSufficientScopes && (Date.now() >= cached.expires)) {
            logger.info(`getAccessToken: access token expired, refreshing...`);
            return (await this.refreshToken(cached)).access;
        }

        logger.warn(`getAccessToken: insufficient scopes in cached token, launching new authentication`);
        return (await this.authenticateNewCharacter(_.compact(_.concat(cached.scopes, scopes)) as Scope[])).access;
    }

    public async authenticateNewCharacter(scopes: Scope[]): Promise<IToken> {
        const { oauthEndpoint, clientId, hookServerPort } = this.config.esi.auth;
        const state = String(_.random(Number.MAX_VALUE));
        const authParams = {
            state,
            response_type: 'code',
            redirect_uri: `http://localhost:${hookServerPort}/auth-hook`,
            client_id: clientId,
            scope: scopes.join(' '),
        };

        logger.info(chalk.yellow(`Starting user authorization process with state=${state}`));
        await opn(`${oauthEndpoint}/authorize?${querystring.stringify(authParams)}`);
        const code = await this.waitAuthCode(state);
        return this.requestToken(scopes, code);
    }

    private async requestToken(scopes: Scope[], code: string): Promise<IToken> {
        const { oauthEndpoint, clientId, secretKey } = this.config.esi.auth;
        const loginParams = {
            code,
            grant_type: 'authorization_code',
        };

        const authHeader = `Basic ${Buffer.from(`${clientId}:${secretKey}`).toString('base64')}`;
        const tokenResponse = await got.post(`${oauthEndpoint}/token`, {
            headers: {
                Authorization: authHeader,
                Host: 'login.eveonline.com',
            },
            form: true,
            json: true,
            body: loginParams,
        });

        const token: IToken = this.parseTokenResponse(tokenResponse.body, scopes);

        await this.authDao.update(token.id, token, { upsert: true });
        return token;
    }

    private async refreshToken(cached: IToken): Promise<IToken> {
        const { oauthEndpoint, clientId, secretKey } = this.config.esi.auth;

        const loginParams = {
            refresh_token: cached.refresh,
            grant_type: 'refresh_token',
            scope: cached.scopes.join(' '),
        };

        const tokenResponse = await got.post(`${oauthEndpoint}/token`, {
            headers: {
                Authorization: `Basic ${Buffer.from(`${clientId}:${secretKey}`).toString('base64')}`,
                Host: url.parse(oauthEndpoint).hostname,
            },
            form: true,
            json: true,
            body: loginParams,
        });
        const refreshedToken: IToken = this.parseTokenResponse(tokenResponse.body, cached.scopes as Scope[]);
        await this.authDao.update(cached.id, refreshedToken, { upsert: true });
        return refreshedToken;
    }

    private async waitAuthCode(state: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const timer = setTimeout(() => {
                _.remove(this.awaitedHooks, { state });
                logger.warn(`waitAuthCode: timeout for state=${state}`);
                reject(new Error(`auth hook timeout for state=${state}`));
            }, this.config.esi.auth.authTimeout);

            this.awaitedHooks.push({ state, timer, resolve });
        });
    }

    private onAuthCodeReceived: IHookCallback = (response: IHookResponse) => {
        const awaitedHook = _.find(this.awaitedHooks, { state: response.state });
        if (!awaitedHook) {
            logger.error('onAuthCodeReceived: unknown or expired hook code received', response);
            return;
        }

        logger.info('onAuthCodeReceived: hook triggered with', response);
        clearTimeout(awaitedHook.timer);
        awaitedHook.resolve(response.code);
        _.remove(this.awaitedHooks, { state: response.state });
    }

    private parseTokenResponse(response: unknown, scopes: Scope[]): IToken {
        if (typeof response !== 'object') {
            throw new Error(`Can not parse token response ${JSON.stringify(response)}`);
        }
        logger.info('parsing token reponse', response);
        const { access_token, expires_in, refresh_token } = response as any;
        if (!access_token || !_.isString(access_token)
                || !expires_in || !_.isNumber(expires_in)
                || !refresh_token || !_.isString(refresh_token)) {
            throw new Error(`Invalid token response ${JSON.stringify({ access_token, expires_in, refresh_token })}`);
        }

        const decoded = jwt(access_token);
        const characterId = decoded.sub.match(/:([^:]*)$/)[1];

        console.log(`Expires in ${expires_in}ms`);

        return {
            scopes,
            id: parseInt(characterId),
            access: access_token,
            expires: Date.now() + expires_in * 1000,
            refresh: refresh_token,
        };
    }
}
